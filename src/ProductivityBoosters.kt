import java.time.LocalDate
import java.time.chrono.ChronoLocalDate

// Named Arguments
fun format(userName: String, domain: String) = "$userName@$domain"

// Destructuring Declarations
fun findMinMax(list: List<Int>): Pair<Int, Int> {
    return Pair(50, 100)
}

// Smart Casts
fun main() {
    // Named Arguments
    println(format("mario", "example.com"))
    println(format("domain.com", "username"))
    println(format(userName = "foo", domain = "bar.com"))
    println(format(domain = "frog.com", userName = "pepe"))

    // String Templates
    val greeting = "Phubet Sukpeung"
    println("Hello $greeting")
    println("Hello ${greeting.toUpperCase()}")

    // Destructuring Declarations
    val (x, y, z) = arrayOf(5, 10, 15)
    val map = mapOf("Alice" to 21, "Bob" to 25)
    for ((name, age) in map) {
        println("$name is $age years old")
    }
}