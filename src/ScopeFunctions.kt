// let
fun customPrint(s: String) {
    print(s.toUpperCase())
}

// with
class Configuration(var host: String, var port: Int)

// apply
data class Person(var name: String, var age: Int, var about: String) {
    constructor() : this("", 0, "")
}

// also
fun writeCreationLog(p: Person) {
    println("A new person ${p.name} was created.")
}

fun main() {
    // let
    val empty = "".let {
        customPrint(it)
        it.isEmpty()
    }
    println(" is empty: $empty")

    fun printNonNull(str: String?) {
        println("Printing \"$str\":")

        str?.let {
            print("\t")
            customPrint(it)
            println()
        }
    }
    printNonNull(null)
    printNonNull("my string")

    // with
    val configuration = Configuration(host = "127.0.0.1", port = 9000)
    with(configuration) {
        println("$host:$port")
    }
    println("${configuration.host}:${configuration.port}")

    // apply
    val jake = Person()
    val stringDescription = jake.apply {
        name = "Jake"
        age = 30
        about = "Android developer"
    }.toString()
    println(stringDescription)

    // also
    val jake1 = Person("Jake", 30, "Android developer")
            .also {
                writeCreationLog(it)
            }
}