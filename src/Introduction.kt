import java.lang.Integer.sum

//fun main(args: Array<String>){
//    println("Hello World")
//}

fun printmsg(message: String): Unit {
    println(message)
}
fun printmsgPrefix(message: String, prefix: String = "Sukpeung") {
    println("[$prefix] $message")
}

fun printAllWithPrefix(vararg messages: String, prefix: String) {
    for (m in messages) println(prefix + m)
}

// Null Safety
fun describeString(maybeString: String?): String {
    if (maybeString != null && maybeString.length > 0) {
        return "String of length ${maybeString.length}"
    } else {
        return "Empty or null string"
    }
}

// Class
class Customer
class Contact(val id: String, var name: String)

// Generic
class MutableStack<E>(vararg items: E) {
    private val elements = items.toMutableList()
    fun push(element: E) = elements.add(element)
    fun peek(): E = elements.last()
    fun pop(): E = elements.removeAt(elements.size - 1)
    fun isEmpty() = elements.isEmpty()
    fun size() = elements.size
    override fun toString() = "MutableStack(${elements.joinToString()})"
}

open class Dog {
    open fun sayHello() {
        println("Hello!")
    }
}

class Yorkshire : Dog() {
    override fun sayHello() {
        println("wif wif!")
    }
}

fun main() {
    // Hello World
    printmsg("Phubet")
    printmsgPrefix("Benz","Benzzz")
    printmsgPrefix("Phubet")
    printmsgPrefix(prefix = "Sukpeung", message = "Phubet")
    println(sum(5,5))

    // Functions
    infix fun Int.times(str: String) = str.repeat(this)
    println(4 times "Bye ")

    val pair = "CS" to "BUU"
    println(pair)

    fun printAll(vararg messages: String) {
        for (m in messages) print(m+" ")
    }
    printAll("A", "B", "C", "D", "E")
    println()

    printAllWithPrefix(
            "A", "B", "C", "D", "E",
            prefix = "Greeting: "
    )

    // Variables
    var a: String = "initial"
    println(a)
    val b: Int = 1
    println(b+3)

    val d: Int
    if (true) {
        d = 7
    } else {
        d = 2
    }
    println(d)

    // Null Safety
    println(describeString(null))

    // Class
    val customer = Customer()
    val contact = Contact("1", "Phubet")
    println(contact.id+". "+contact.name)

    // Generics
    val stack = MutableStack(1,3,5,7)
    stack.push(9)
    println(stack)
    println("peek(): ${stack.peek()}")
    for (i in 1..stack.size()) {
        println("pop(): ${stack.pop()}")
        println(stack)
    }

    // Inheritance
    val dog: Dog = Yorkshire()
    val testdog: Dog = Dog()
    dog.sayHello()
    testdog.sayHello()


}