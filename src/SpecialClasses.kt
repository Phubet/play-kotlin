import java.util.*

data class User(val name: String, val surname: String)

// Enum
enum class State {
    IDLE, RUNNING, FINISHED
}

// Object Keyword
class LuckDispatcher {
    fun getNumber() {
        var objRandom = Random()
        println(objRandom.nextInt(90))
    }
}


fun main() {
    //Data Classes
    val user = User("Alex", "R9")
    println(user)

    val secondUser = User("Alex", "R9")
    val thirdUser = User("Max", "Phu")

    println("user == secondUser: ${user == secondUser}")
    println("user == thirdUser: ${user == thirdUser}")
    println(user.hashCode())
    println(thirdUser.hashCode())

    // Enum
    val state = State.IDLE
    val message = when (state) {
        State.IDLE -> "IDLE"
        State.RUNNING -> "RUNNING"
        State.FINISHED -> "FINISHED"
    }
    println(message)

    // Object Keyword
    val d1 = LuckDispatcher()
    val d2 = LuckDispatcher()
    d1.getNumber()
    d2.getNumber()
}